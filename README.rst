=================
Extractor Service
=================

A Rejected consumer that subscribes to video events published from the converter API service.
It extracts audio from video file and stores audio file into mongodb

:Repository:    `<https://gitlab.com/microservices5894911/python_tornado/video_converter/extractor>`_
:Docker Tags:   `<https://hub.docker.com/repository/docker/mvijayaragavan/extractor/>`_


Environment Variables
=====================

Required
--------
- ``AMQP_URL``:  The AMQP connection string used to emit events and RPC messages.
- ``MONGO_URL``: Connection string used to connect to the mongo database.
- ``REJECTED_CONSUMERS``: Consumer information


Quickstart Development Guide
============================

Setting up your environment
---------------------------
.. code-block:: bash

   $ python3.9 -m venv env
   $ . ./env/bin/activate
   $ pip install -e '.[docs,tests]'
   $ ./bootstrap.sh
   $ . ./build/test-environment

Testing the application in dev
------------------------------

.. code-block:: bash

	(env) $ flake8
	(env) $ coverage run && coverage report

Building
--------
.. code-block:: bash

	(env) $ docker build .

Running Locally
---------------
This project uses the `rejected`_ AMQP consumer framework which includes
a supervisory process manager.  Running the consumer locally uses the
``rejected`` command that is installed into the development environment.

.. code-block:: bash

	(env) $ rejected -f -c build/rejected.yaml

This will run the consumer in the foreground and connect to the local environment's RabbitMQ server.

.. _rejected: https://rejected.readthedocs.io/


Using in Another Development Environment
----------------------------------------
This service publishes a docker image to `https://hub.docker.com/repository/docker/mvijayaragavan/extractor/`_ so
integrating it into an existing docker-based development environment is simple.

At the time of writing the following docker-compose snippet is what is required:

.. code-block:: yaml

    %YAML 1.2
    ---
    version: "3.0"
    services:
      mongo:
        image: mongo:latest
        ports:
          - 27017
        environment:
          MONGO_INITDB_ROOT_USERNAME: root
          MONGO_INITDB_ROOT_PASSWORD: rootpassword

      extractor:
        image: mvijayaragavan/extractor:latest
        environment:
          AMQP_URL: amqp://guest:guest@rabbitmq:5672/%2F
          MONGO_URL: mongodb://root:rootpassword@mongo:27017
          REJECTED_CONSUMERS: '{"audio_extractor":{"ack":true,"connections":["rabbitmq"],"consumer":"extractor_consumer.consumer.ExtractorConsumer","error_max_retry":3,"max_errors":100,"qos_prefetch":1,"qty":2,"queue":"videos"}}'

      rabbitmq:
        image: rabbitmq:3-management-alpine
        ports:
          - 5672
          - 15672
