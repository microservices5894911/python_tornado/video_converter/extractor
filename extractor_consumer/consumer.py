import asyncio
import json
import os
import tempfile
import uuid

import moviepy.editor
from motor.motor_asyncio import AsyncIOMotorGridFSBucket
from rejected import consumer
from tornado import gen

from extractor_consumer import mongo

MAX_STREAMED_SIZE = 1024 * 1024 * 1024


class ExtractorConsumer(consumer.Consumer):
    def initialize(self):
        super().initialize()
        asyncio.get_event_loop().run_until_complete(self.connect_db())

    async def connect_db(self):
        """Connect to Postgres database"""
        mongo.install(self,
                      mongo_database=os.getenv('MONGO_DATABASE', 'converter'))

    @gen.coroutine
    def process(self):
        job = json.loads(self.body)

        video_id = job.get('video_fid', None)
        if not video_id:
            raise consumer.MessageException('video file not found - %r' %
                                            video_id)

        video_name = job.get('video_name', None)
        # read video from db
        video_tmp = yield self.read_video_from_db(video_id)

        # extract audio from video
        audio = moviepy.editor.VideoFileClip(video_tmp.name).audio
        video_tmp.close()

        # write audio to audio_temp file
        audio_temp = tempfile.gettempdir() + f"/{video_id}.mp3"
        audio.write_audiofile(audio_temp)

        # save audio to db
        audio_id = str(uuid.uuid4())
        audio_name = audio_id + '.mp3'
        yield self.save_audio_to_db(audio_id, audio_name, video_id, video_name,
                                    audio_temp)

        message = {'video_fid': video_id, 'mp3_fid': audio_id}
        os.unlink(video_tmp.name)
        os.unlink(audio_temp)

        self.publish_message('converter', 'audio',
                             {'content_type': 'application/json'},
                             json.dumps(message))

    def read_in_chunks(self, file_object, chunk_size=MAX_STREAMED_SIZE):
        """Lazy function (generator) to read a file piece by piece.
        Default chunk size: 1k."""
        while True:
            data = file_object.read(chunk_size)
            if not data:
                break
            yield data

    async def read_video_from_db(self, video_id):
        fs = AsyncIOMotorGridFSBucket(self.motor_client.video)
        video_temp = tempfile.NamedTemporaryFile(delete=False)

        file = open(video_temp.name, 'wb+')
        await fs.download_to_stream(video_id, file)
        return video_temp

    async def save_audio_to_db(self, audio_id, audio_name, video_id,
                               video_name, audio_path):
        fs = AsyncIOMotorGridFSBucket(self.motor_client.audio)
        async with fs.open_upload_stream_with_id(
                file_id=audio_id,
                filename=audio_name,
                metadata={
                    'contentType': 'text/octet-stream',
                    'video_id': video_id,
                    'video_name': video_name
                }) as gridin:
            with open(audio_path, 'rb') as f:
                for piece in self.read_in_chunks(f):
                    await gridin.write(piece)
