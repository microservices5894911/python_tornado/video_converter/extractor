FROM mvijayaragavan/rejected:3.22.0-0

LABEL url="https://gitlab.com/microservices5894911/python_tornado/video_converter/extractor"

COPY dist/*.tar.gz /tmp/

RUN apk --virtual devdeps add gcc musl-dev linux-headers g++ libpq postgresql-dev \
 && apk --no-cache add ffmpeg \
 && pip install --no-cache-dir /tmp/*.tar.gz \
 && apk del --purge devdeps \
 && rm -rf /var/cache/apk/*
