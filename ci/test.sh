#!/usr/bin/env sh
# testing script for the CI pipeline
set -ex

pip install -e '.[tests]'

rm -rf build && mkdir -p build

echo 'Running flake8...'
if ! flake8 --output-file=build/flake8.txt
then
    echo 'ERROR: flake8 detected PEP8 violations'
    cat build/flake8.txt
    echo ''
    exit 1
fi

echo 'Running yapf...'
if ! yapf -dr extractor_consumer tests >build/yapf.diff
then
    echo 'ERROR: yapf detected formatting violations'
    cat build/yapf.diff
    echo ''
    exit 1
fi

echo "Running bootstrap..."
./bootstrap.sh
. ./build/test-environment

echo "Running tests..."
coverage run
coverage report
coverage xml
