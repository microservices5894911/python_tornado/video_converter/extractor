import json
import uuid

import rejected.consumer
from rejected import testing

import tests
from extractor_consumer import consumer


class ExtractorTests(testing.AsyncTestCase):
    def setUp(self):
        super().setUp()

    def get_consumer(self):
        return consumer.ExtractorConsumer

    @testing.gen_test
    async def test_extractor(self):
        file_id = str(uuid.uuid4())
        await tests.add_mock_video(self.consumer.motor_client, file_id)
        message = {
            'video_fid': file_id,
            'video_name': 'test_video1.mp4',
            'mp3_fid': None
        }
        await self.process_message(message_body=message)
        result = self.published_messages[0]
        self.assertEqual(result.exchange, 'converter')
        self.assertEqual(result.routing_key, 'audio')
        res = json.loads(result.body)
        self.assertEqual(res['video_fid'], file_id)
        self.assertIsNotNone(res['mp3_fid'])

    @testing.gen_test
    async def test_extractor_without_video_id(self):
        message = {'mp3_fid': None}
        with self.assertRaises(rejected.consumer.MessageException):
            await self.process_message(message_body=message)
