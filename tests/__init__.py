import os
from urllib.parse import quote

from motor.motor_asyncio import AsyncIOMotorGridFSBucket


async def add_mock_video(motor_client, file_id):
    filename = './test_videos/test_video1.mp4'
    fs = AsyncIOMotorGridFSBucket(motor_client.video)
    url_path = quote(os.path.basename(filename))

    with open(filename, 'rb') as binary_file:
        data = binary_file.read()

    await fs.upload_from_stream_with_id(file_id, url_path, data)
